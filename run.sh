#!/bin/bash

function green {
    echo -e "\e[1;32m"$1"\e[0m"
}

function red {
    echo -e "\e[1;31m"$1"\e[0m"
}

function yellow {
    echo -e "\e[1;33m"$1"\e[0m"
}

green "[*] Running Problem $(basename $0)..."

if go run src/$0.go; then
green "[*] Program exit with code $?"
else
red "[!] Program exit with code $?"
fi

extra_tokens=$(cat | wc -w)
if [[ $extra_tokens != "0" ]]; then
    yellow "[!] $extra_tokens extra tokens found"
fi
