package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func solve(cin *scanner, cout *writer, cerr *writer) {
}

func main() {
	cin := NewScanner(os.Stdin)
	cout, cerr := NewWriter(os.Stdout), NewWriter(os.Stderr)
	defer cout.Flush()
	defer cerr.Flush()
	t := cin.NextInt()
	// t := 1
	for ti := 0; ti < t; ti++ {
		solve(cin, cout, cerr)
	}
}

// Template Bolierplate {{{
type scanner struct {
	*bufio.Scanner
}

func NewScanner(r io.Reader) *scanner {
	s := bufio.NewScanner(r)
	s.Split(bufio.ScanWords)
	s.Buffer(nil, 1024*1024)
	return &scanner{s}
}

func (i *scanner) Next() string {
	i.Scan()
	return i.Text()
}

func (i *scanner) Nexts(n int) []string {
	res := make([]string, n)
	for j := range res {
		res[j] = i.Next()
	}
	return res
}

func (i *scanner) NextInt() int {
	v, _ := strconv.Atoi(i.Next())
	return v
}

func (i *scanner) NextInt64() int64 {
	v, _ := strconv.ParseInt(i.Next(), 10, 64)
	return v
}

func (i *scanner) NextInts(n int) []int {
	res := make([]int, n)
	for j := range res {
		res[j] = i.NextInt()
	}
	return res
}

func (i *scanner) NextInt64s(n int) []int64 {
	res := make([]int64, n)
	for j := range res {
		res[j] = i.NextInt64()
	}
	return res
}

type writer struct {
	w *bufio.Writer
}

func NewWriter(w io.Writer) *writer {
	return &writer{bufio.NewWriter(w)}
}

func (i *writer) Print(a ...interface{}) {
	fmt.Fprint(i.w, a...)
}

func (i *writer) Println(a ...interface{}) {
	fmt.Fprintln(i.w, a...)
}

func (i *writer) Printf(format string, a ...interface{}) {
	fmt.Fprintf(i.w, format, a...)
}

func (i *writer) Flush() {
	i.w.Flush()
}

// }}}

// vim: fdm=marker
